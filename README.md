# Repository for shared figures with CC

## Content

- forest_dieback directory : Artist view of a forest dieback model. From equations in [Ritchie et al. 2021](https://doi.org/10.1038/s41586-021-03263-2) and inspired by a picture in [Model Calendar 2015](https://www.stockholmresilience.org/download/18.3110ee8c1495db744326951/1459560294940/Model+calendar+2015+digital.pdf) designed by Elsa Wikander at Azote. © Ludovic Mailleret.
- ceratits_capitata directory : stereomicroscope photo of two *Ceratits capitata* individuals (males). © Cécile Bresch, Kevan Rastello; post-processing: Xavier Fauvergue.
